// 前端工具 utilities
import Swal from 'sweetalert2'
import 'libs/clicker_box'
const utilities = {
	loadingAnimationTime: 500,
	windowsWidth: window.innerWidth,
	thresholdDesktop: 992,
	gridGutter: 15,
	getCookie (name) {
		let eachArray,
			cookieArray = document.cookie.split(';');

		for (let key in cookieArray) {
			eachArray = cookieArray[key].split('=');
			if (name === eachArray[0].trim()) {
				return eachArray[1];
			}
		}
		return null;
	},
	setCookie (name, value) {
		document.cookie = `${name}=${value}; path=/`
	},
	alert(msg, btntext="OK") {
		Swal.fire({
			title: '',
			html:
				`
			<span class="alert__item icon--alert--ok"></span>
			<p class="alert__item title--alert">${msg}</p>
			<p class="text-center"><span id="btn__alert__close" class="d-inline-block text-uppercase btn__alert--confirm btn--base02 pointer">${btntext}</span></p>
			`,
			showConfirmButton: false,
			customClass: {
				actions: 'mt-2 mb-3',
			},
			onBeforeOpen: () => {
				btn__alert__close.addEventListener('click', () => {
					Swal.close()
				})
			}
		})
	},
	// click and add class
	triggerClass(triggerName, className, minPx, maxPx, addTarget=false ) {
        var $triggerBtn = $(triggerName);
        if($triggerBtn.length > 0) {
            $triggerBtn.clicker_box({
                ACTIVE: className, // 切換內容
                ATR: "class", // 操作參數
                TARGET: addTarget, // 操作目標 (false,.class,#id)
                RANGE: [minPx,maxPx], // 依照解析度啟動 (最小,最大)
            });
        }
	},
	// 前導動畫開始
	startPageLoading(callback = null) {
		//-----------------------------------------------
		// 前導動畫 開始
		// document ready , 前導動畫
		const bodyElement = document.querySelector('.body__with__pageloader');
		const preLoadElement = document.querySelector('.pageload');

		if(bodyElement) {
			// 如果有前導動畫
			bodyElement.classList.add('is--pageloading')
		} else {
			console.log('無前導', preLoadElement)
			if(preLoadElement)
				preLoadElement.style.display = 'none !important';
			if(callback) {
				callback();
			}
		}
		// 前導動畫 結束
		//-----------------------------------------------
	},
	// 前導動畫消失
	endPageLoading(callback = null) {
		//-----------------------------------------------
		// 結束前導動畫page前導動畫
		//-----------------------------------------------
		const bodyElement = document.querySelector('.body__with__pageloader');
		const preLoadElement = document.querySelector('.pageload');
		if(bodyElement) {
			// 如果有前導動畫
			setTimeout(function(){
				bodyElement.classList.remove('is--pageloading');
				bodyElement.classList.add('is--pageloaded');
				if(callback) {
					callback();
				}
			}, utilities.loadingAnimationTime);
			setTimeout(function(){
				preLoadElement.style.display = 'none !important';
			}, utilities.loadingAnimationTime + 300);
		} else {
			if(preLoadElement) {
				preLoadElement.style.display = 'none !important';
			}
			if(callback) {
				callback();
			}
		}
		//-----------------------------------------------
	},
	//-----------------------------------------------
	// header
	//-----------------------------------------------
	header: {
		init() {
			utilities.megamenu.megaMenuShowData();
			utilities.megamenu.megamenuPosWidth_Wide();
			utilities.megamenu.megamenuPosWidth_Full();
			this.btn__mobile__mainmenu_click();
		},
		btn__mobile__mainmenu_click() {
			const mobileMainMenuButton = document.querySelector('.js__btn-mobile-mainmenu');
			mobileMainMenuButton.addEventListener('click', () => {
				let target = document.querySelector('.body');
				if(!mobileMainMenuButton.classList.contains('js__btn-mobile-mainmenu--active')) {
					mobileMainMenuButton.classList.add('js__btn-mobile-mainmenu--active')
					if (!target.classList.contains('mainmenu--opened')) {
						target.classList.add('mainmenu--opened')
					}
				} else {
					mobileMainMenuButton.classList.remove('js__btn-mobile-mainmenu--active')
					target.classList.remove('mainmenu--opened');
				}
			});
		}
	},
	//-----------------------------------------------
	// megamenu
	//-----------------------------------------------
	megamenu: {
		// thresholdDesktop: utilities.thresholdDesktop,
		megaMenuShowData() {
			//Show dropdown on hover only for desktop devices
			//-----------------------------------------------
			if (utilities.windowsWidth >= utilities.thresholdDesktop) {
				$('.mega-menu-wrap:not(.onclick) .mega-menu-nav>.dropdown, .mega-menu-wrap:not(.onclick) .dropdown>.dropdown-menu>.dropdown').hover(
					function () {
						$(this).addClass('show');
						$(this).find('>.dropdown-menu').addClass('show');
					},
					function () {
						$(this).removeClass('show');
						$(this).find('>.dropdown-menu').removeClass('show');
					});
			};
		},

		//-----------------------------------------------
		// megamenuPosWidth_Wide: position and width for mega-menu--wide
		//-----------------------------------------------
		megamenuPosWidth_Wide() {
			if ($('.mega-menu-wrap .mega-menu--wide').length > 0 && utilities.windowsWidth > utilities.thresholdDesktop - 1) {
				var $dropdownMenu = $('.mega-menu--wide > .dropdown-menu'); // target
				var headerSecondLeft = parseInt($dropdownMenu.closest('.dropdown').offset().left),
					headerFirstLeft = parseInt($('.mega-menu-wrap').offset().left),
					megaMenuLeftPosition = headerFirstLeft - headerSecondLeft;
				$dropdownMenu.css('left', megaMenuLeftPosition + 'px');
				console.log('left = ', megaMenuLeftPosition);
				$(window).resize(function () {
					var headerSecondLeft = parseInt($('.mega-menu-wrap').offset().left),
						headerFirstLeft = parseInt($('.mainheader-mainmenu-wrap').offset().left),
						megaMenuLeftPosition = headerFirstLeft - headerSecondLeft;
					$dropdownMenu.css('left', megaMenuLeftPosition + 'px');
					console.log('resize, left = ', megaMenuLeftPosition);
				});
			}
		},
		//-----------------------------------------------
		// megamenuPosWidth_Full: position and width for mega-menu--wide
		//-----------------------------------------------
		megamenuPosWidth_Full() {
			//Mega menu full width
			if ($('.mega-menu-wrap .mega-menu--full').length > 0 && utilities.windowsWidth > utilities.thresholdDesktop - 1) {
				var $dropdownMenu = $('.mega-menu--full > .dropdown-menu'); // target
				var headerSecondLeft = parseInt($('.mega-menu-wrap').offset().left + utilities.gridGutter),
					headerFirstLeft = parseInt($('.mainheader-mainmenu-wrap').offset().left),
					megaMenuLeftPosition = headerFirstLeft - headerSecondLeft,
					megaMenuWidth = parseInt($('.mainheader').width());
				$dropdownMenu.css('left', megaMenuLeftPosition + 'px');
				$dropdownMenu.css('width', megaMenuWidth + 'px');
				$(window).resize(function () {
					var headerSecondLeft = parseInt($('.mega-menu-wrap').offset().left + utilities.gridGutter),
						headerFirstLeft = parseInt($('.mainheader-mainmenu-wrap').offset().left),
						megaMenuLeftPosition = headerFirstLeft - headerSecondLeft,
						megaMenuWidth = parseInt($('.mainheader').width());
					var $dropdownMenu = $('.mega-menu--full > .dropdown-menu');
					$dropdownMenu.css('left', megaMenuLeftPosition + 'px');
					$dropdownMenu.css('width', megaMenuWidth + 'px');
				});
			}
		}
		//-----------------------------------------------
	},
	//-----------------------------------------------
	// init global
	//-----------------------------------------------
	initGlobalFunction() {
		this.header.init();
	},
};

export default utilities;