// import 'libs/fontawesome-free-5.8.2-web/css/all.min.css'
import 'page-home.sass';
import utilities from 'utilities';

// slider start ----------------------------------------------
import Swiper from 'swiper';
import 'swiper/css/swiper.min.css';
// slider end ----------------------------------------------

document.addEventListener('DOMContentLoaded', (event) => {
    utilities.startPageLoading(()=>{
        utilities.initGlobalFunction();
        var swiper = new Swiper('.swiper-container');
    });
})

window.addEventListener('load', (event) => {
    utilities.endPageLoading();
})