var path = require("path");
var webpack = require('webpack');
const MiniCssExtractPlugin = require("mini-css-extract-plugin");
var CopyWebpackPlugin = require('copy-webpack-plugin');

module.exports = {
    mode: process.env.NODE_ENV,
    // 指定所有entry的資料夾
    context: path.resolve(__dirname, "src"),
    entry: {
        // index: 'index.js', 以下為extensions所產生縮寫
        index: 'index',
        page404: 'page404'
    },
    output : {
        // 依照entry的name修正output檔名
        path: path.resolve(__dirname, "dist"),
        filename: 'js/[name].js'
    },
    optimization: {
        splitChunks: {
            cacheGroups: {
                vendor: {
                    test: /node_modules/,
                    name: 'vendor',
                    chunks: 'initial',
                    enforce: true
                }
            }
        }
    },
    devServer: {
        compress: true,
        port: 3001,
        stats: {
            assets: true,
            cached: false,
            chunkModules: false,
            chunkOrigins: false,
            chunks: false,
            colors: true,
            hash: false,
            modules: false,
            reasons: false,
            source: false,
            version: false,
            warnings: false
        }
    },
    plugins: [
        // 全域，不需要在各個entry獨立引入jquery
        // 建議少用providePlugin
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        // filename: output
        new MiniCssExtractPlugin({
            filename: 'css/[name].css'
        }),

        // 原封不動copy
        new CopyWebpackPlugin([{
            from: 'images',
            to: 'images'
        }]),
        new CopyWebpackPlugin([{
            from: 'libs',
            to: 'libs'
        }]),
        new CopyWebpackPlugin([{
            from: 'upload_files',
            to: 'upload_files'
        }])
    ],
    resolve: {
        // entry 可以省略路徑
        modules: [
            path.resolve('src'),
            path.resolve('src/js'),
            path.resolve('src/sass'),
            path.resolve('src/images'),
            path.resolve('src/libs'),
            path.resolve('src/upload_files'),
            path.resolve('node_modules')
        ],
        // entry 可以省略副檔名
        extensions: ['.js']
    },
    module: {
        rules: [
            {
                test: /\.css$/,
                exclude: /libs/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: "../"
                        }
                    },
                    'css-loader',
                    'postcss-loader'
                ]
            },
            {
                test: /\.(sa|sc)ss$/,
                exclude: /libs/,
                use: [
                    {
                        loader: MiniCssExtractPlugin.loader,
                        options: {
                            publicPath: "../"
                        }
                    },
                    'css-loader',
                    'postcss-loader',
                    'sass-loader',
                ]
            },
            {
                test: /\.js$/,
                exclude: /bower_components/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            },
            {
                test: /\.(jpe?g|png|gif)$/,
                use: [{
                    loader: 'file-loader',
                    options: {
                        name: '[path][name].[ext]'
                    }
                }]
            },
            {
                // sass等有引入assets檔案時，需透過『file-loader』判別副檔案
                test: /\.(woff|woff2|ttf|eot)$/,
                loader: 'file-loader',
                options: {
                    name: '[path][name].[ext]',
                    outputPath: 'fonts/'
                }
            },
            {
                test: /\.svg$/,
                exclude: /upload/,
                use: [
                    {
                        loader: 'file-loader',
                        options: {
                            name: '[path][name].[ext]',
                            publicPath: '../'
                        }
                    }
                ]
            }
        ]
    }
}